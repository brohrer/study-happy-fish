import numpy as np
from y04_array_viz import Array3DAnimation

pts = Array3DAnimation("wave")

vals = np.ones((7, 8, 9))

i_rows = np.cumsum(vals, axis=0)
i_cols = np.cumsum(vals, axis=1)
i_channel = np.cumsum(vals, axis=2)

for i_step in range(3000):
    # vals = np.cos(
    #     .03 * i_step +
    #     .5 * (i_rows**2 + i_cols**2 + i_channel**2)**.5)
    vals = np.maximum(0, np.cos(
        .03 * i_step +
        .4 * (i_rows**2 + (1.3 * i_cols)**2 + (1.1 * i_channel)**2)**.5) - .7)
    pts.update(vals)
