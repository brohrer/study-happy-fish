import numpy as np
import matplotlib.pyplot as plt

fig = plt.figure(figsize=(4, 8))
ax = fig.add_axes((0, 0, 1, 1))
ax.set_xlim(0, 1)
ax.set_ylim(0, 1)

ts = np.linspace(0, 1, 100)
xs = (np.sin(ts * 10) + 1) * .5
ys = np.ones(ts.size) * .1
plt.ion()
plt.show()

fish, = ax.plot(0, 0, markersize=30, marker='^')
food_start = .3
food_started = False

for t, x, y in zip(ts, xs, ys):
    fish.set_xdata(x)
    fish.set_ydata(y)

    if not food_started:
        if t > food_start:
            food, = ax.plot(.25, 1.1, markersize=30, marker='o')
            food_started = True
    if food_started:
        food.set_ydata(food.get_ydata() - .03)

    fig.canvas.flush_events()
