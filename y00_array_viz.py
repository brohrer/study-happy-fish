import numpy as np
import matplotlib.pyplot as plt

orange = "#ff6d69"
gold = "#fecc50"
light_blue = "#0be7fb"
dark_blue = "#010b8b"
dark = "#1e0521"


class Array2DAnimation:
    """
    Show a two dimensional array as it evolves over time.
    This is a way to track how an array's values evolve over time,
    especially during the development of an online algorithm
    or streaming signal processing method.

    Typical usage:

        from y00_array_viz import Array2DAnimation
        pts = Array2DAnimation("window title")
        # Then each iteration
        pts.update(vals)

    """
    def __init__(self, name="2 dimensional array"):
        self.fig = None
        self.name = name

    def initialize(self, data):
        fig_width = 2
        fig_height = fig_width
        self.fig = plt.figure(
            figsize=(fig_width, fig_height),
            num=self.name,
            facecolor=dark,
            edgecolor=dark,
            dpi=300,
        )
        border = .1
        self.ax = self.fig.add_axes((
            border, border, 1 - 2 * border, 1 - 2 * border),
            frameon=False,
        )
        self.ax.tick_params(
            bottom=False,
            top=False,
            left=False,
            right=False)
        self.ax.tick_params(
            labelbottom=False,
            labeltop=False,
            labelleft=False,
            labelright=False)

        n_rows, n_cols = data.shape
        data_size = np.maximum(n_rows, n_cols)
        # The largest allowed marker diameter, in inches
        self.max_marker_size = (
            np.minimum(fig_width, fig_height) *
            (1 - 2 * border) /
            data_size *
            .77  # Max fraction of space a marker can occupy
        )

        self.ax.set_xlim(0, data_size + 1)
        self.ax.set_ylim(data_size + 1, 0)

        data_center = (data_size + 1) / 2
        x_center = (n_cols - 1) / 2
        xs = np.arange(n_cols) - x_center + data_center
        x_tiled = np.tile(xs[np.newaxis, :], (n_rows, 1))
        y_center = (n_rows - 1) / 2
        ys = np.arange(n_rows) - y_center + data_center
        y_tiled = np.tile(ys[:, np.newaxis], (1, n_cols))

        sizes = scatter_size(np.abs(data) * self.max_marker_size)
        self.positive_elements = self.ax.scatter(
            x_tiled.ravel(),
            y_tiled.ravel(),
            c=gold,
            edgecolors="none",
            s=sizes.ravel(),
        )
        self.negative_elements = self.ax.scatter(
            x_tiled.ravel(),
            y_tiled.ravel(),
            c=light_blue,
            edgecolors="none",
            s=sizes.ravel(),
        )

        # Manually make some row and column number labels
        n_labels_max = 13
        n_skip = data_size // 13
        x_left = -1 - x_center + data_center
        x_right = n_cols - x_center + data_center
        for i_row in range(n_rows):
            if (i_row % n_skip) != 0:
                continue
            y = i_row - y_center + data_center
            self.ax.text(
                x_left,
                y,
                str(i_row),
                horizontalalignment="right",
                verticalalignment="center",
                color=orange,
                fontsize=3,
                fontfamily="sans-serif",
            )
            self.ax.text(
                x_right,
                y,
                str(i_row),
                horizontalalignment="left",
                verticalalignment="center",
                color=orange,
                fontsize=3,
                fontfamily="sans-serif",
            )

        y_top = -1 - y_center + data_center
        y_bottom = n_rows - y_center + data_center
        for i_col in range(n_cols):
            if (i_col % n_skip) != 0:
                continue

            x = i_col - x_center + data_center
            self.ax.text(
                x,
                y_top,
                str(i_col),
                horizontalalignment="center",
                verticalalignment="bottom",
                color=orange,
                fontsize=3,
                fontfamily="sans-serif",
            )
            self.ax.text(
                x,
                y_bottom,
                str(i_col),
                horizontalalignment="center",
                verticalalignment="top",
                color=orange,
                fontsize=3,
                fontfamily="sans-serif",
            )

        plt.ion()
        plt.show()

    def update(self, new_data):
        if self.fig is None:
            self.initialize(new_data)

        data = new_data.ravel() / np.max(np.abs(new_data))
        positive_data = np.maximum(0, data)
        negative_data = np.maximum(0, -data)
        positive_sizes = scatter_size(positive_data * self.max_marker_size)
        negative_sizes = scatter_size(negative_data * self.max_marker_size)
        self.positive_elements.set_sizes(positive_sizes)
        self.negative_elements.set_sizes(negative_sizes)

        self.fig.canvas.flush_events()


def scatter_size(diameters):
    """
    Convert a set of diameters in inches to Matplotlib scatterplot sizes,
    diameter in points, squared.
    """
    diameters_in_points = diameters * 72
    sizes = diameters_in_points ** 2
    return sizes
