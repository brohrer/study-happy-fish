import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches


class Fish:
    def __init__(self, ax):
        self.n_actions = 2
        # action[0]: move upward 1 unit
        # action[1]: move upward .5 units
        # action[2]: move upward .25 units
        # action[3]: move downward 1 unit
        # action[4]: move downward .5 units
        # action[5]: move downward .25 units

        self.body_path = np.array([
            [6.5, -.3],
            [6, 1.3],
            [4.5, 2.1],
            [1.2, 2.3],
            [-2.2, 1.7],
            [-4.5, .5],
            [-6, 1.6],
            [-7, .2],
            [-6, -1.5],
            [-5, -.5],
            [-3, -2.7],
            [2.4, -3],
            [5.9, -2.1],
            [6.2, -1.3],
            [5.5, -.9],
        ])

        self.eye_path = np.array([
            [4.8, .78],
            [5.1, .5],
            [5.3, .7],
            [5.29, .9],
            [5.07, 1.15],
        ])

        self.dorsal_path = np.array([
            [4, 1.6],
            [3.1, 2.8],
            [.4, 3],
            [-3, 2],
            [-4.3, 0],
        ])

        self.flipper_path = np.array([
            [1.7, -.3],
            [1.4, .4],
            [0, -.4],
            [1.3, -.9],
        ])

        self.dorsal_poly = patches.Polygon(self.dorsal_path, facecolor="black")
        dorsal_patch = ax.add_patch(self.dorsal_poly)

        self.body_poly = patches.Polygon(self.body_path)
        body_patch = ax.add_patch(self.body_poly)

        self.eye_poly = patches.Polygon(self.eye_path, facecolor="black")
        eye_patch = ax.add_patch(self.eye_poly)

        self.flipper_poly = patches.Polygon(self.flipper_path, facecolor="black")
        flipper_patch = ax.add_patch(self.flipper_poly)

        self.x = .5
        self.y = .5
        self.s = .05  # scale
        self.draw()

        self.attempted_movement = 0

        self.eating_distance = .2

    def step(self, actions):
        actions = np.maximum(0, np.minimum(1, actions))
        self.attempted_movement = (
            actions[0] - actions[1])
        return self.attempted_movement

    def draw(self):
        self.dorsal_poly.set_xy(
            self.dorsal_path * self.s +
            np.array([self.x, self.y])[np.newaxis, :])
        self.body_poly.set_xy(
            self.body_path * self.s +
            np.array([self.x, self.y])[np.newaxis, :])
        self.eye_poly.set_xy(
            self.eye_path * self.s +
            np.array([self.x, self.y])[np.newaxis, :])
        self.flipper_poly.set_xy(
            self.flipper_path * self.s +
            np.array([self.x, self.y])[np.newaxis, :])
