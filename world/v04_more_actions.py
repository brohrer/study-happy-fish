import numpy as np
import matplotlib.pyplot as plt
from world.f02_more_actions import Fish

# palette from
# https://sarahrenaeclark.com/color-palettes-ocean-life/
coral = "#fc4f00"
salmon = "#ff7062"
dark_blue = "#245c81"
medium_blue = "#338fb8"
light_blue = "#80ddef"


class World:
    def __init__(self):
        """
        Set up the visualization and define the sensors and reward.
        """
        # Lay out the size and shape of things.
        left_border = .15
        right_border = .15
        bottom_border = .15
        top_border = .15
        fish_world_height = 2
        fish_world_width = 3

        total_width = fish_world_width + left_border + right_border
        total_height = fish_world_height + top_border + bottom_border

        # Create the figure objects.
        self.fig = plt.figure(
            figsize=(total_width, total_height),
            facecolor=medium_blue)
        self.ax = self.fig.add_axes((
            left_border / total_width,
            bottom_border / total_height,
            fish_world_width / total_width,
            fish_world_height / total_height))

        self.i_step = 0
        self.i_sim_step = 0
        # Number of times to run through the simulation loop before updating
        # the visualization.
        self.drawing_interval = 50
        # Number of simulation steps per iteration of the top level loop.
        self.n_sim_steps = 13

        # Intialize the fish.
        self.fish = Fish(self.ax)
        self.fish_y_bottom = .5
        self.fish_y_top = 1.5
        if np.random.sample() < .5:
            self.fish.y = self.fish_y_bottom
        else:
            self.fish.y = self.fish_y_top

        self.food_pieces = []
        # The fraction of time steps on which new food is created.
        self.food_fraction = .1 / self.n_sim_steps

        # Define how the world will interpret commands.
        self.n_actions = 6
        self.actions = np.zeros(self.n_actions)
        # action[0]: move upward 1 unit
        # action[1]: move upward .5 units
        # action[2]: move upward .25 units
        # action[3]: move downward 1 unit
        # action[4]: move downward .5 units
        # action[5]: move downward .25 units

        # A time sequence of micro-actions to execute, one per simulated
        # time step.
        self.action_buffer = [np.zeros(self.n_actions)] * 20

        # Count the sensors and lay out the sensor array.
        self.n_depths = 6
        self.n_laterals = 3
        self.n_bumps = 2
        self.n_walls = 2
        self.n_sensors = (
            self.n_depths * self.n_laterals + self.n_bumps + self.n_walls)
        self.sensors = np.zeros(self.n_sensors)

        # Define all the sources and sizes of reward and punishment.
        self.reward = 0
        self.wall_bump_penalty = -.1
        self.eating_reward = 1
        self.missed_food_penalty = -.3
        self.action_penalty = -.01

        # Draw the world.
        self.ax.set_xlim(0, fish_world_width)
        self.ax.set_ylim(0, fish_world_height)
        self.ax.set_facecolor(dark_blue)
        self.ax.spines["top"].set_linewidth(3)
        self.ax.spines["bottom"].set_linewidth(3)
        self.ax.spines["left"].set_linewidth(.5)
        self.ax.spines["right"].set_linewidth(.5)
        self.ax.spines["top"].set_color("black")
        self.ax.spines["bottom"].set_color("black")
        self.ax.spines["left"].set_color("black")
        self.ax.spines["right"].set_color("black")
        self.ax.tick_params(bottom=False, top=False, left=False, right=False)
        self.ax.tick_params(
            labelbottom=False,
            labeltop=False,
            labelleft=False,
            labelright=False)

        plt.ion()
        plt.show()
        self.fish.draw()

    def step(self, actions=None):
        """
        Take one step forward in time.
        Create and destroy Food as appropriate.
        """
        self.i_step += 1
        self.actions = actions
        if self.actions is None:
            self.actions = np.zeros(self.n_actions)
        self.action_buffer[0] = self.actions


        # Reset sensors and reward for the new iteration.
        self.reward = 0
        self.sensors = np.zeros(self.n_sensors)

        # Carry out any actions.
        wall_bottom = 0
        wall_top = 0
        bump_bottom = 0
        bump_top = 0

        for _ in range(self.n_sim_steps):
            # Update action buffer
            self.i_sim_step += 1
            microaction = self.action_buffer.pop(0)
            self.action_buffer.append(np.zeros(self.n_actions))
            attempted_movement = self.fish.step(microaction)
            self.fish.y += attempted_movement

            # Wall proximity sensors
            if self.fish.y < .5:
                wall_bottom = 1
            if self.fish.y > 1.5:
                wall_top = 1

            # Wall bump sensors
            if self.fish.y < 0:
                bump_bottom = 1
                self.reward += self.wall_bump_penalty
                self.fish.y = 0
            if self.fish.y > 2:
                bump_top = 1
                self.reward += self.wall_bump_penalty
                self.fish.y = 2

            self.reward += attempted_movement * self.action_penalty
            # Create new food.
            if np.random.sample() < self.food_fraction:
                self.food_pieces.append(Food(self.ax))

            # Move the food along.
            for food in self.food_pieces:
                food.step()

            # Destroy old food.
            for food in self.food_pieces:
                if food.x < 0:
                    self.reward += self.missed_food_penalty
                    food.remove()
                    self.food_pieces.remove(food)

            # Update the food position sensors.
            # Break the world into a coarse occupancy grid, with
            # the fish at the bottom center.
            distance_resolution = .5
            depth_resolution = 1
            n_occupancy = self.n_depths * self.n_laterals
            occupancy = np.zeros(n_occupancy)
            for food in self.food_pieces:
                dy = food.y - self.fish.y
                dx = food.x - self.fish.x

                i_col = int(
                    np.floor((dy + depth_resolution / 2) / depth_resolution) +
                    self.n_laterals // 2)
                i_col = np.minimum(self.n_laterals - 1, np.maximum(0, i_col))

                i_row = int(dx / distance_resolution)
                i_row = np.minimum(self.n_depths - 1, np.maximum(0, i_row))

                i_cell = i_row + i_col * self.n_depths
                occupancy[i_cell] = 1

            # Integrate the sensor values over the duration of the
            # top loop iteration.
            self.sensors[:n_occupancy] += occupancy / self.n_sim_steps
            i_next = n_occupancy
            self.sensors[i_next] += wall_bottom / self.n_sim_steps
            i_next += 1
            self.sensors[i_next] += wall_top / self.n_sim_steps
            i_next += 1
            self.sensors[i_next] += bump_bottom / self.n_sim_steps
            i_next += 1
            self.sensors[i_next] += bump_top / self.n_sim_steps

            # Let the fish eat if there's food nearby.
            for food in self.food_pieces:
                distance_to_food = np.sqrt(
                    (food.y - self.fish.y) ** 2 +
                    (food.x - self.fish.x) ** 2)
                if distance_to_food <= self.fish.eating_distance:
                    self.reward += self.eating_reward
                    food.remove()
                    self.food_pieces.remove(food)

            # Update the plan view of the world.
            if self.i_sim_step % self.drawing_interval == 0:
                self.fish.draw()
                for food in self.food_pieces:
                    food.draw()
                self.fig.canvas.flush_events()

        return self.sensors.copy(), self.reward


class Food:
    def __init__(self, ax):
        if np.random.sample() < .5:
            self.y = .5
        else:
            self.y = 1.5
        self.x = 3.5
        self.x_step_size = .5 / 13

        markersize = 10
        self.food_drawing, = ax.plot(
            self.x,
            self.y,
            color=light_blue,
            markersize=markersize,
            marker='o',
        )

    def step(self):
        self.x -= self.x_step_size

    def draw(self):
        self.food_drawing.set_xdata(self.x)

    def remove(self):
        self.x = -1
        self.food_drawing.remove()
