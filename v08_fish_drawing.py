import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches

body_path = [
    [6.5, -.3],
    [6, 1.3],
    [4.5, 2.1],
    [1.2, 2.3],
    [-2.2, 1.7],
    [-4.5, .5],
    [-6, 1.6],
    [-7, .2],
    [-6, -1.5],
    [-5, -.5],
    [-3, -2.7],
    [2.4, -3],
    [5.9, -2.1],
    [6.2, -1.3],
    [5.5, -.9],
]

eye_path = [
    [4.8, .78],
    [5.1, .5],
    [5.3, .7],
    [5.29, .9],
    [5.07, 1.15],
]

dorsal_path = [
    [4, 1.6],
    [3.1, 2.8],
    [.4, 3],
    [-3, 2],
    [-4.3, 0],
]

flipper_path= [
    [1.7, -.3],
    [1.4, .4],
    [0, -.4],
    [1.3, -.9],
]

fig = plt.figure()
ax = fig.add_axes((.1, .1, .8, .8))

dorsal_patch = ax.add_patch(patches.Polygon(
    dorsal_path,
    facecolor="black",
))
body_patch = ax.add_patch(patches.Polygon(body_path))
eye_patch = ax.add_patch(patches.Polygon(
    eye_path,
    facecolor="black",
))
flipper_patch = ax.add_patch(patches.Polygon(
    flipper_path,
    facecolor="black",
))

ax.set_xlim(-40, 40)
ax.set_ylim(-30, 30)
# ax.axis("equal")
# ax.grid()
plt.show()
