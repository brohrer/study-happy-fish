import numpy as np
import matplotlib.pyplot as plt
from v00_array_vis import Array2DAnimation, Array3DAnimation

left_border = 1
right_border = .8
bottom_border = 1
top_border = .8

fish_world_height = 3
fish_world_width = 2

total_width = history_width + left_border + right_border
total_height = fish_world_height + top_border + bottom_border

fish_bottom = bottom_border

markersize = 20

# palette from
# https://sarahrenaeclark.com/color-palettes-ocean-life/
coral = "#fc4f00"
salmon = "#ff7062"
dark_blue = "#245c81"
medium_blue = "#338fb8"
light_blue = "#80ddef"


class Vis:
    def __init__(self):

    def step(self, mind, world):


    def show(self, mind,_world):


    fig = plt.figure(
        figsize=(total_width, total_height),
        facecolor=medium_blue)
    fish_ax = fig.add_axes((
        left_border / total_width,
        fish_bottom / total_height,
        fish_world_width / total_width,
        fish_world_height / total_height))
    world = FishWorld(fish_ax)

    plt.ion()
    plt.show()

    fig.canvas.flush_events()


class FishWorld:
    def __init__(self, ax):
        self.ax = ax

        # Intialize the fish.
        self.fish = Fish(self.ax)
        self.fish_x_left = .5
        self.fish_x_right = 1.5
        if np.random.sample() < .5:
            self.fish.x = self.fish_x_left
        else:
            self.fish.x = self.fish_x_right

        self.food_pieces = []
        # The fraction of time steps on which new food is created.
        self.food_fraction = .1

        self.action_threshold = .1
        self.n_actions = 2
        self.actions = np.zeros(self.n_actions)
        # action[0]: move to the right
        # action[1]: move to the left

        self.n_depths = 6
        self.n_laterals = 3
        self.n_bumps = 2
        self.n_walls = 2
        self.n_sensors = (
            self.n_depths * self.n_laterals + self.n_bumps + self.n_walls)
        self.sensors = np.zeros(self.n_sensors)

        self.reward = 0
        self.wall_bump_penalty = -.1
        self.eating_reward = 1
        self.missed_food_penalty = -.3
        self.action_penalty = -.01

        # Draw the world.
        self.ax.set_xlim(0, fish_world_width)
        self.ax.set_ylim(0, fish_world_height)
        self.ax.set_facecolor(dark_blue)
        self.ax.spines["top"].set_linewidth(.5)
        self.ax.spines["bottom"].set_linewidth(.5)
        self.ax.spines["left"].set_linewidth(6)
        self.ax.spines["right"].set_linewidth(6)
        self.ax.spines["top"].set_color("black")
        self.ax.spines["bottom"].set_color("black")
        self.ax.spines["left"].set_color("black")
        self.ax.spines["right"].set_color("black")
        self.ax.tick_params(bottom=False, top=False, left=False, right=False)
        self.ax.tick_params(
            labelbottom=False,
            labeltop=False,
            labelleft=False,
            labelright=False)
        self.fish.draw()

    def execute_actions(self, actions=None):
        """
        Also create and destroy Food as appropriate.
        """
        # If no actions provided, generate some.
        self.actions = actions
        if self.actions is None:
            self.actions = np.zeros(self.n_actions)
            for i_action in range(self.n_actions):
                if np.random.sample() < self.action_threshold:
                    self.actions[i_action] = 1

        # Reset sensors and reward for the new iteration.
        self.reward = 0
        self.sensors = np.zeros(self.n_sensors)

        # Carry out any actions.
        wall_left = 0
        wall_right = 0
        bump_left = 0
        bump_right = 0

        attempted_movement = self.fish.step(self.actions)
        self.fish.x += attempted_movement

        # Wall proximity sensors
        if self.fish.x < 1:
            wall_left = 1
        else:
            wall_right = 1

        # Wall bump sensors
        if self.fish.x < 0:
            bump_left = 1
            self.reward += self.wall_bump_penalty
            self.fish.x = self.fish_x_left
        if self.fish.x > 2:
            bump_right = 1
            self.reward += self.wall_bump_penalty
            self.fish.x = self.fish_x_right

        self.reward += np.sum(self.actions) * self.action_penalty
        # Create new food.
        if np.random.sample() < self.food_fraction:
            self.food_pieces.append(Food(self.ax))

        # Move the food along.
        for food in self.food_pieces:
            food.step()

        # Destroy old food.
        for food in self.food_pieces:
            if food.y < 0:
                self.reward += self.missed_food_penalty
                food.remove()
                self.food_pieces.remove(food)

        # Update the food position sensors.
        # Break the world into a coarse occupancy grid, with
        # the fish at the bottom center.
        depth_resolution = .5
        lateral_resolution = 1
        n_occupancy = self.n_depths * self.n_laterals
        occupancy = np.zeros(n_occupancy)
        for food in self.food_pieces:
            dx = food.x - self.fish.x
            dy = food.y - self.fish.y

            i_col = int(
                np.floor((dx + lateral_resolution / 2) / lateral_resolution) +
                self.n_laterals // 2)
            i_col = np.minimum(self.n_laterals - 1, np.maximum(0, i_col))

            i_row = int(dy / depth_resolution)
            i_row = np.minimum(self.n_depths - 1, np.maximum(0, i_row))

            i_cell = i_row + i_col * self.n_depths
            occupancy[i_cell] = 1

        self.sensors[:n_occupancy] = occupancy
        i_next = n_occupancy
        self.sensors[i_next] = wall_left
        i_next += 1
        self.sensors[i_next] = wall_right
        i_next += 1
        self.sensors[i_next] = bump_left
        i_next += 1
        self.sensors[i_next] = bump_right

        # Let the fish eat if there's food nearby.
        for food in self.food_pieces:
            distance_to_food = np.sqrt(
                (food.x - self.fish.x) ** 2 +
                (food.y - self.fish.y) ** 2)
            if distance_to_food <= self.fish.eating_distance:
                self.reward += self.eating_reward
                food.remove()
                self.food_pieces.remove(food)

        # Update the plan view of the world.
        self.fish.draw()
        for food in self.food_pieces:
            food.draw()

        return self.sensors.copy(), self.reward


class Fish:
    def __init__(self, ax):
        self.x = 0
        self.y = .5
        self.action_threshold = .5
        self.attempted_movement = 0

        self.eating_distance = .2
        self.fish_drawing, = ax.plot(
            self.x,
            self.y,
            color=coral,
            markersize=markersize,
            marker='^',
        )

    def step(self, actions):
        actions = np.maximum(0, np.minimum(1, actions))
        actions = actions - np.min(actions)
        self.attempted_movement = 0
        if actions[0] - actions[1] > self.action_threshold:
            self.attempted_movement = -1
        if actions[1] - actions[0] > self.action_threshold:
            self.attempted_movement = 1
        return self.attempted_movement

    def draw(self):
        self.fish_drawing.set_xdata(self.x)


class Food:
    def __init__(self, ax):
        if np.random.sample() < .5:
            self.x = .5
        else:
            self.x = 1.5
        self.y = 3.5
        self.y_step_size = .5

        self.food_drawing, = ax.plot(
            self.x,
            self.y,
            color=light_blue,
            markersize=markersize,
            marker='o',
        )

    def step(self):
        self.y -= self.y_step_size

    def draw(self):
        self.food_drawing.set_ydata(self.y)

    def remove(self):
        self.y = -1
        self.food_drawing.remove()


class Monitor:
    def __init__(self, ax):
        self.ax = ax
        self.ax.set_facecolor(light_blue)
        self.n_pts = 1000
        self.history = list(np.zeros(self.n_pts))
        self.timeline = np.arange(self.n_pts) - self.n_pts + 1
        self.line, = self.ax.plot(
            np.array(self.history),
            color="black",
            linewidth=.5)
        self.ax.set_xlim(self.timeline[1], self.timeline[-1])
        self.ax.set_ylim(-1, 1)
        self.ax.set_ylabel("reward")
        # self.ax.set_xlabel("time steps")
        self.ax.grid()

    def update(self, new_performance):
        self.history.pop(0)
        self.history.append(new_performance)
        self.timeline += 1
        self.ax.set_xlim(self.timeline[1], self.timeline[-1])
        self.line.set_data(self.timeline, np.array(self.history))


class Lifetime:
    def __init__(self, ax):
        self.ax = ax
        self.ax.set_facecolor(light_blue)
        self.batch_size = 1000
        self.performance_min_magnitude = .01
        self.performance_batch = []
        self.history = [0]
        self.timeline = [0]
        self.line, = self.ax.plot(
            np.array(self.timeline),
            np.array(self.history),
            color="black",
            linewidth=.5)
        self.ax.set_xlim(0, self.batch_size)
        self.ax.set_ylim(
            -self.performance_min_magnitude,
            self.performance_min_magnitude)
        self.ax.set_ylabel("reward")
        self.ax.set_xlabel("time steps")
        self.ax.grid()

    def update(self, new_performance):
        self.performance_batch.append(new_performance)
        if len(self.performance_batch) == self.batch_size:
            batch_performance = np.mean(np.array(self.performance_batch))
            self.history.append(batch_performance)
            self.performance_batch = []
            self.timeline.append(
                int(self.batch_size * (len(self.history) - 1)))
            self.ax.set_xlim(self.timeline[0], self.timeline[-1])
            self.ax.set_ylim(
                np.minimum(
                    np.min(self.history),
                    -self.performance_min_magnitude),
                np.maximum(
                    np.max(self.history),
                    self.performance_min_magnitude))
            self.line.set_data(np.array(self.timeline), np.array(self.history))


class Predictions:
    def __init__(self, ax):
        self.ax = ax
        self.ax.set_facecolor(light_blue)
        self.batch_size = 1000
        self.performance_min_magnitude = .01
        self.performance_batch = []
        self.history = [0]
        self.timeline = [0]
        self.line, = self.ax.plot(
            np.array(self.timeline),
            np.array(self.history),
            color="black",
            linewidth=.5)
        self.ax.set_xlim(0, self.batch_size)
        self.ax.set_ylim(
            -self.performance_min_magnitude,
            self.performance_min_magnitude)
        self.ax.set_ylabel("reward")
        self.ax.set_xlabel("time steps")
        self.ax.grid()

    def update(self, new_performance):
        self.performance_batch.append(new_performance)
        if len(self.performance_batch) == self.batch_size:
            batch_performance = np.mean(np.array(self.performance_batch))
            self.history.append(batch_performance)
            self.performance_batch = []
            self.timeline.append(
                int(self.batch_size * (len(self.history) - 1)))
            self.ax.set_xlim(self.timeline[0], self.timeline[-1])
            self.ax.set_ylim(
                np.minimum(
                    np.min(self.history),
                    -self.performance_min_magnitude),
                np.maximum(
                    np.max(self.history),
                    self.performance_min_magnitude))
            self.line.set_data(np.array(self.timeline), np.array(self.history))
