import numpy as np
from y00_array_viz import Array2DAnimation

pts = Array2DAnimation("test display")

shape = (55, 77)

for _ in range(1000):
    vals = np.random.normal(size=shape)
    pts.update(vals)
