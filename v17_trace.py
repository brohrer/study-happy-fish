from mind.v02_trace import Mind
from world.v06_position_sensing import World

world = World()
mind = Mind(world.n_actions, world.n_sensors)

# Initialize the actions to "do nothing" for the first time through the loop.
actions = None
n_steps = 3e5
for i_step in range(int(n_steps)):
    sensors, reward = world.step(actions)
    actions = mind.step(sensors, reward)
