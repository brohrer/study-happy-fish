import pickle as pkl
from mind.v02_trace import Mind
from world.v08_flappy_bird import World

n_steps = 1e5

mind_filename = "mind.bak"
world = World()
try:
    # First try to load a saved mind.
    with open(mind_filename, "rb") as f:
        mind = pkl.load(f)
except Exception:
    # If that's not successful, create one from scratch.
    mind = Mind(
        world.n_actions,
        world.n_sensors,
        trace_length=8,
        decay_rate=.2)

# Initialize the actions to "do nothing" for the first time through the loop.
actions = None
for i_step in range(int(n_steps)):
    sensors, reward = world.step(actions)
    actions = mind.step(sensors, reward)

# Save a backup copy of the mind.
with open(mind_filename, "wb") as f:
    pkl.dump(mind, f)
