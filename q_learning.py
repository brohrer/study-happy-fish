import numpy as np


class QLearning:
    """
    An implementation of Q-Learning
    https://en.wikipedia.org/wiki/Q-learning
    """
    def __init__(
        self,
        discount_factor=.9,
        exploration_rate=.01,
        initial_q=0,
        learning_rate=.01,
        n_actions=None,
        trace_length=10,
    ):
        """
        discount_factor, float
        Between 0 and 1. .8 to .95 are typical values.
        Determines how much each time step discounts
        the value of future rewards. 0 means only the current time step
        is considered. 1 means all rewards are counted the same,
        no matter how far in the future they occur.

        exploration_rate, float
        Between 0 and 1. .001 to .1 are typical.
        Determines what fraction of time steps are exploratory, that is,
        actions are chosen at random, rather than based on the
        highest expected reward.

        initial_q, float
        The initial estimate of the value for every state-action pair.
        A value of greater than 0 gives the algorithm a bit of optimism.
        It assumes that actions it has never tried before will have
        a mildly positive result.

        learning rate, float
        Between 0 and 1. 1e-5 to 1e-2 are typical.
        Determines how rapidly the value estimate for each state-action
        combination (Q-table) adapts to each new observation.

        n_actions, int
        The number of discrete actions to choose from.
        The outcome of each pass will be an array of zeros of this size,
        with a single 1, indicating which action to take.
        In practice, this implemention will add one more action:
        the do-nothing action. If this is selected, then the action array
        returned will be all zeros.

        trace_legnth, int
        The number of time steps over which to accumulate reward for
        evaluating the outcome of an action.
        """
        self.discount_factor = discount_factor
        self.exploration_rate = exploration_rate
        self.initial_q = initial_q
        self.learning_rate = learning_rate
        self.trace_length = trace_length

        if n_actions is None:
            print("The number of possible actions must be specified")
            print("with the n_actions keyword argument.")
            raise RuntimeError
        self.n_actions = n_actions

        # Initialize the state-action value table as a dictionary.
        # Dictionary keys will be state arrays.
        # Dictionary values will be the Q-values,
        # the expected reward associated with each action.
        self.q_table = {}
        self.actions = np.array(self.n_actions)

        self.action_history = []
        self.reward_history = []
        self.state_history = []
        self.discount_schedule = (
            self.discount_factor ** np.arange(self.trace_length))

        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def __str__(self):
        str_parts = [
            "Q-Learning",
            f"discount factor (gamma): {self.discount_factor}",
            f"exploration rate (epsilon): {self.exploration_rate}",
            f"learning rate (alpha): {self.learning_rate}",
            f"number of actions: {self.n_actions}",
            f"trace length: {self.trace_length}",
        ]
        return "\n".join(str_parts)

    def forward_pass(self, forward_in):
        """
        Choose an action based on the current state.
        """
        self.forward_in = forward_in
        self.state = str(self.forward_in[0])
        self.reward = self.forward_in[1]

        if self.state in self.q_table:
            if np.random.sample() <= self.exploration_rate:
                # Choose an action at random.
                self.i_action = np.random.randint(self.n_actions + 1)
            else:
                # Choose the action that has historically had the best outcome.
                self.i_action = np.argmax(self.q_table.get(self.state))
        else:
            self.q_table[self.state] = (
                np.ones(self.n_actions + 1) * self.initial_q)
            # Choose an action at random.
            self.i_action = np.random.randint(self.n_actions + 1)

        self.state_history.append(self.state)
        self.reward_history.append(self.reward)
        self.action_history.append(self.i_action)

        return self.forward_out

    def backward_pass(self, backward_in):
        """
        Update the value table
        """
        self.backward_in = backward_in

        if len(self.reward_history) == self.trace_length:
            discounted_reward = np.sum(
                np.array(self.reward_history) * self.discount_schedule)

            self.reward_history.pop(0)
            state_to_update = self.state_history.pop(0)
            action_to_update = self.action_history.pop(0)
            old_value = self.q_table[state_to_update][action_to_update]
            new_value = (
                old_value * (1 - self.learning_rate) +
                discounted_reward * self.learning_rate)
            self.q_table[state_to_update][action_to_update] = new_value

        # Truncate the final action. It's the do-nothing action.
        action = np.zeros(self.n_actions)
        if self.i_action < self.n_actions:
            action[self.i_action] = 1
        self.backward_out = action
        return self.backward_out
