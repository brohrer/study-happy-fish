import numpy as np
from PIL import Image
from y00_array_viz import Array2DAnimation

pts = Array2DAnimation("pup portrait")
img = np.asarray(Image.open("pup_01.jpg"))
n_shrink = 20
gray_img = (
    .299 * img[::n_shrink, ::n_shrink, 0] +
    .587 * img[::n_shrink, ::n_shrink, 1] +
    .114 * img[::n_shrink, ::n_shrink, 2])
for _ in range(1000):
    pts.update(gray_img)
