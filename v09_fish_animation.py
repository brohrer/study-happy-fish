import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches

body_path = np.array([
    [6.5, -.3],
    [6, 1.3],
    [4.5, 2.1],
    [1.2, 2.3],
    [-2.2, 1.7],
    [-4.5, .5],
    [-6, 1.6],
    [-7, .2],
    [-6, -1.5],
    [-5, -.5],
    [-3, -2.7],
    [2.4, -3],
    [5.9, -2.1],
    [6.2, -1.3],
    [5.5, -.9],
])

eye_path = np.array([
    [4.8, .78],
    [5.1, .5],
    [5.3, .7],
    [5.29, .9],
    [5.07, 1.15],
])

dorsal_path = np.array([
    [4, 1.6],
    [3.1, 2.8],
    [.4, 3],
    [-3, 2],
    [-4.3, 0],
])

flipper_path= np.array([
    [1.7, -.3],
    [1.4, .4],
    [0, -.4],
    [1.3, -.9],
])

fig = plt.figure()
ax = fig.add_axes((.1, .1, .8, .8))

dorsal_poly = patches.Polygon(dorsal_path, facecolor="black")
dorsal_patch = ax.add_patch(dorsal_poly)

body_poly = patches.Polygon(body_path)
body_patch = ax.add_patch(body_poly)

eye_poly = patches.Polygon( eye_path, facecolor="black")
eye_patch = ax.add_patch(eye_poly)

flipper_poly = patches.Polygon( flipper_path, facecolor="black")
flipper_patch = ax.add_patch(flipper_poly)

ax.set_xlim(-40, 40)
ax.set_ylim(-30, 30)
plt.ion()
plt.show()

for t in np.arange(10000):
    x = -15 * np.cos(t / 40)
    y = -10 * np.sin(t / 14)
    s = 1 + .3 * np.cos(t / 83)

    dorsal_poly.set_xy(dorsal_path * s + np.array([x, y])[np.newaxis, :])
    body_poly.set_xy(body_path * s + np.array([x, y])[np.newaxis, :])
    eye_poly.set_xy(eye_path * s + np.array([x, y])[np.newaxis, :])
    flipper_poly.set_xy(flipper_path * s + np.array([x, y])[np.newaxis, :])
    fig.canvas.flush_events()
