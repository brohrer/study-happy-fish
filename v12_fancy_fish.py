from mind.v00_mind import Mind
from world.v02_fancy_fish import World

world = World()
mind = Mind(world.n_actions, world.n_sensors)

# Initialize the actions to "do nothing" for the first time through the loop.
actions = None
n_steps = 3e3
for i_step in range(int(n_steps)):
    sensors, reward = world.step(actions)
    actions = mind.step(sensors, reward)
