import numpy as np
import matplotlib.pyplot as plt

fig = plt.figure()
ax = fig.add_axes((0, 0, 1, 1))
ax.set_xlim(0, 1)
ax.set_ylim(0, 1)

xs = np.linspace(0, 1, 100)
ys = (np.sin(xs * 10) + 1) * .5
plt.ion()
plt.show()

fish, = ax.plot(0, 0, markersize=10, marker='^')

for x, y in zip(xs, ys):
    fish.set_xdata(x)
    fish.set_ydata(y)
    fig.canvas.flush_events()
