import numpy as np


class Canopy:
    def __init__(self, n_actions, n_sensors):
        # Account for 1 action to be the "do nothing" action.
        self.n_actions = n_actions + 1
        # Account for 2 sensors to represent positive and negative reward.
        self.n_sensors = n_sensors + 2

        # Include space for reward as a sensor.
        self.recent_sensors = np.zeros(self.n_sensors)
        self.sensors = np.zeros(self.n_sensors)
        self.actions = np.zeros(self.n_actions)
        self.prefixes = None
        self.sequences = None
        self.probabilities = None

    def initialize(self):
        # Include space for reward as a sensor.
        self.prefixes = np.ones((self.n_sensors, self.n_actions))
        self.sequences = np.zeros((
            self.n_sensors, self.n_actions, self.n_sensors))
        self.probabilities = np.zeros((
            self.n_sensors, self.n_actions, self.n_sensors))

    def conditional_predict(self):
        # Collapse model into conditional prediction
        current_probabilities = (
            self.probabilities * self.sensors[:, np.newaxis, np.newaxis])
        conditional_predictions = np.max(current_probabilities, axis=0)

        uncertainties = 1 / self.prefixes
        return conditional_predictions, uncertainties, self.sensors

    def predict(self, actions):
        # Collapse model into conditional prediction
        current_probabilities = (
            self.probabilities *
            self.sensors[:, np.newaxis, np.newaxis] *
            self.actions[np.newaxis, :, np.newaxis])
        predictions = np.max(np.max(current_probabilities, axis=1), axis=0)

        return predictions

    def update_probabilities(self):
        self.probabilities = self.sequences / self.prefixes[:, :, np.newaxis]

    def update_actions(self, actions):
        if self.sequences is None:
            self.initialize()
        no_actions = np.maximum(0, 1 - np.sum(actions))
        self.actions = np.array([no_actions] + list(actions))

    def update_sensors(self, sensors, reward):
        if self.sequences is None:
            self.initialize()
        self.recent_sensors = self.sensors
        positive_reward = np.minimum(1, np.maximum(0, reward))
        negative_reward = np.minimum(1, np.maximum(0, -reward))
        self.sensors = np.array(
            [positive_reward, negative_reward] + list(sensors))

        # Update the state-action transition count.
        current_prefix = (
            self.recent_sensors[:, np.newaxis] *
            self.actions[np.newaxis, :])
        self.prefixes += current_prefix

        # Update the state-action-state transition count.
        current_sequence = (
            self.recent_sensors[:, np.newaxis, np.newaxis] *
            self.actions[np.newaxis, :, np.newaxis] *
            self.sensors[np.newaxis, np.newaxis, :])
        self.sequences += current_sequence
