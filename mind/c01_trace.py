import numpy as np

default_trace_length = 3
default_decay_rate = .3


class Canopy:
    def __init__(
        self,
        n_actions,
        n_sensors,
        trace_length=None,
        decay_rate=None,
    ):
        """
        n_actions, int
        n_sensors, int
        The number of actions and sensors that the world will be expecting
        and providing. These are determined by the nature of the world.

        trace_length, int
        How many steps back to assign credit for reward.

        decay_rate, float
        Between zero and one, typically closer to zero.
        What fraction of the reward gets decayed away with each time step.
        """
        # Account for 1 action to be the "do nothing" action.
        self.n_actions = n_actions + 1
        # Account for 2 sensors to represent positive and negative reward.
        self.n_sensors = n_sensors + 2

        if decay_rate is None:
            self.decay_rate = decay_rate
        else:
            self.decay_rate = default_decay_rate

        if trace_length is None:
            self.trace_length = trace_length
        else:
            self.trace_length = default_trace_length

        # Include space for reward as a sensor.
        self.sensors = np.zeros(self.n_sensors)
        self.delayed_sensors = np.zeros(self.n_sensors)
        self.recent_delayed_sensors = np.zeros(self.n_sensors)
        self.actions = np.zeros(self.n_actions)
        self.delayed_actions = np.zeros(self.n_actions)
        self.sensor_trace = [np.zeros(self.n_sensors)] * trace_length
        self.action_trace = [np.zeros(self.n_actions)] * trace_length
        self.reward_trace = [0] * trace_length

        self.prefixes = None
        self.sequences = None
        self.probabilities = None

    def initialize(self):
        # Include space for reward as a sensor.
        self.prefixes = np.ones((self.n_sensors, self.n_actions))
        self.sequences = np.zeros((
            self.n_sensors, self.n_actions, self.n_sensors))
        self.probabilities = np.zeros((
            self.n_sensors, self.n_actions, self.n_sensors))

    def conditional_predict(self):
        # Collapse model into conditional prediction
        current_probabilities = (
            self.probabilities * self.sensors[:, np.newaxis, np.newaxis])
        conditional_predictions = np.max(current_probabilities, axis=0)

        uncertainties = 1 / self.prefixes
        return conditional_predictions, uncertainties, self.sensors

    def predict(self, actions):
        # Collapse model into conditional prediction
        current_probabilities = (
            self.probabilities *
            self.sensors[:, np.newaxis, np.newaxis] *
            self.actions[np.newaxis, :, np.newaxis])
        predictions = np.max(np.max(current_probabilities, axis=1), axis=0)

        return predictions

    def update_probabilities(self):
        self.probabilities = self.sequences / self.prefixes[:, :, np.newaxis]

    def update_actions(self, actions):
        if self.sequences is None:
            self.initialize()
        no_actions = np.maximum(0, 1 - np.sum(actions))
        self.actions = np.array([no_actions] + list(actions))
        self.action_trace.append(self.actions.copy())
        self.action_trace.pop(0)

    def update_sensors(self, sensors, reward):
        if self.sequences is None:
            self.initialize()

        # Show the rewards as 0 until the trace has completed and we can
        # properly sum them up. This shouldn't affect the sensors' ability
        # to make useful predictions in the near term.
        self.sensors = np.array(
            # [positive_reward, negative_reward] + list(sensors))
            [0, 0] + list(sensors))
        self.sensor_trace.append(self.sensors.copy())
        self.sensor_trace.pop(0)
        self.reward_trace.append(reward)
        self.reward_trace.pop(0)

        # print("actions", self.action_trace)
        # print("sensors", self.sensor_trace)
        # print("rewards", self.reward_trace)
        # Compute the full reward trace.
        total_reward = 0
        discount_factor = 1
        for reward_value in self.reward_trace:
            total_reward += reward_value * discount_factor
            discount_factor *= (1 - self.decay_rate)
        positive_reward = np.minimum(1, np.maximum(0, total_reward))
        negative_reward = np.minimum(1, np.maximum(0, -total_reward))

        # Get the delayed actions and sensors for which the trace has
        # been completed.
        self.recent_delayed_sensors = self.delayed_sensors
        self.delayed_sensors = self.sensor_trace[0]
        self.delayed_sensors[0] = positive_reward
        self.delayed_sensors[1] = negative_reward
        self.delayed_actions = self.action_trace[0]

        # Update the state-action transition count.
        current_prefix = (
            self.recent_delayed_sensors[:, np.newaxis] *
            self.delayed_actions[np.newaxis, :])
        self.prefixes += current_prefix

        # Update the state-action-state transition count.
        current_sequence = (
            self.recent_delayed_sensors[:, np.newaxis, np.newaxis] *
            self.delayed_actions[np.newaxis, :, np.newaxis] *
            self.delayed_sensors[np.newaxis, np.newaxis, :])
        self.sequences += current_sequence
