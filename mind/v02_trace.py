from mind.c01_trace import Canopy
from mind.i00_ivy import Ivy
from mind.v01_mind_vis import Vis


class Mind():
    def __init__(
        self,
        n_actions,
        n_sensors,
        trace_length=None,
        decay_rate=None
    ):
        self.model = Canopy(
            n_actions,
            n_sensors,
            trace_length=trace_length,
            decay_rate=decay_rate,
        )
        self.planner = Ivy()
        self.vis = Vis()

    def step(self, sensors, reward):
        self.reward = reward

        self.model.update_sensors(sensors, reward)
        (conditional_predictions, uncertainties, sensors_w_reward
            ) = self.model.conditional_predict()
        actions = self.planner.plan(
            conditional_predictions, uncertainties, sensors_w_reward)
        self.model.update_actions(actions)
        self.model.update_probabilities()
        # predicted_sensors = self.model.predict(actions)

        self.vis.update(self)
        return actions
