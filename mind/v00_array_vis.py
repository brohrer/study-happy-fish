import numpy as np
import matplotlib.pyplot as plt

orange = "#ff6d69"
gold = "#fecc50"
light_blue = "#0be7fb"
dark_blue = "#010b8b"
dark = "#1e0521"


class Array2DAnimation:
    """
    Show a two dimensional array as it evolves over time.
    This is a way to track how an array's values evolve over time,
    especially during the development of an online algorithm
    or streaming signal processing method.

    Typical usage:

        from y00_array_viz import Array2DAnimation
        pts = Array2DAnimation("window title")
        # Then each iteration
        pts.update(vals)

    """
    def __init__(self, name="2 dimensional array"):
        self.fig = None
        self.name = name

    def initialize(self, data):
        fig_width = 2
        fig_height = fig_width
        self.fig = plt.figure(
            figsize=(fig_width, fig_height),
            num=self.name,
            facecolor=dark,
            edgecolor=dark,
            dpi=300,
        )
        border = .1
        self.ax = self.fig.add_axes((
            border, border, 1 - 2 * border, 1 - 2 * border),
            frameon=False,
        )
        self.ax.tick_params(
            bottom=False,
            top=False,
            left=False,
            right=False)
        self.ax.tick_params(
            labelbottom=False,
            labeltop=False,
            labelleft=False,
            labelright=False)

        n_rows, n_cols = data.shape
        data_size = np.maximum(n_rows, n_cols)
        # The largest allowed marker diameter, in inches
        self.max_marker_size = (
            np.minimum(fig_width, fig_height) *
            (1 - 2 * border) /
            data_size *
            .77  # Max fraction of space a marker can occupy
        )

        self.ax.set_xlim(0, data_size + 1)
        self.ax.set_ylim(data_size + 1, 0)

        data_center = (data_size + 1) / 2
        x_center = (n_cols - 1) / 2
        xs = np.arange(n_cols) - x_center + data_center
        x_tiled = np.tile(xs[np.newaxis, :], (n_rows, 1))
        y_center = (n_rows - 1) / 2
        ys = np.arange(n_rows) - y_center + data_center
        y_tiled = np.tile(ys[:, np.newaxis], (1, n_cols))

        sizes = scatter_size(np.abs(data) * self.max_marker_size)
        self.positive_elements = self.ax.scatter(
            x_tiled.ravel(),
            y_tiled.ravel(),
            c=gold,
            edgecolors="none",
            s=sizes.ravel(),
        )
        self.negative_elements = self.ax.scatter(
            x_tiled.ravel(),
            y_tiled.ravel(),
            c=light_blue,
            edgecolors="none",
            s=sizes.ravel(),
        )

        # Manually make some row and column number labels
        n_labels_max = 13
        n_skip = data_size // n_labels_max
        x_left = -1 - x_center + data_center
        x_right = n_cols - x_center + data_center
        for i_row in range(n_rows):
            if (i_row % n_skip) != 0:
                continue
            y = i_row - y_center + data_center
            self.ax.text(
                x_left,
                y,
                str(i_row),
                horizontalalignment="right",
                verticalalignment="center",
                color=orange,
                fontsize=3,
                fontfamily="sans-serif",
            )
            self.ax.text(
                x_right,
                y,
                str(i_row),
                horizontalalignment="left",
                verticalalignment="center",
                color=orange,
                fontsize=3,
                fontfamily="sans-serif",
            )

        y_top = -1 - y_center + data_center
        y_bottom = n_rows - y_center + data_center
        for i_col in range(n_cols):
            if (i_col % n_skip) != 0:
                continue

            x = i_col - x_center + data_center
            self.ax.text(
                x,
                y_top,
                str(i_col),
                horizontalalignment="center",
                verticalalignment="bottom",
                color=orange,
                fontsize=3,
                fontfamily="sans-serif",
            )
            self.ax.text(
                x,
                y_bottom,
                str(i_col),
                horizontalalignment="center",
                verticalalignment="top",
                color=orange,
                fontsize=3,
                fontfamily="sans-serif",
            )

        self.scale_text = self.ax.text(
            0,
            data_size + 1,
            "scale: 0.0",
            horizontalalignment="left",
            verticalalignment="top",
            color=orange,
            fontsize=3,
            fontfamily="sans-serif",
        )

        plt.ion()
        plt.show()

    def update(self, new_data):
        if self.fig is None:
            self.initialize(new_data)

        scale = np.max(np.abs(new_data))
        data = new_data.ravel() / (scale + 1e-20)
        positive_data = np.maximum(0, data)
        negative_data = np.maximum(0, -data)
        positive_sizes = scatter_size(positive_data * self.max_marker_size)
        negative_sizes = scatter_size(negative_data * self.max_marker_size)
        self.positive_elements.set_sizes(positive_sizes)
        self.negative_elements.set_sizes(negative_sizes)
        self.scale_text.set_text(f"scale: {scale:.03}")

        self.fig.canvas.flush_events()


class Array3DAnimation:
    """
    Show a three dimensional array as it evolves over time.
    This is a way to track how an array's values evolve over time,
    especially during the development of an online algorithm
    or streaming signal processing method.

    Typical usage:

        from y00_array_viz import Array3DAnimation
        pts = Array3DAnimation("window title")
        # Then each iteration
        pts.update(vals)

    """
    def __init__(self, name="3 dimensional array"):
        self.fig = None
        self.name = name
        self.i_step = 0
        self.rotation_speed = .03

    def initialize(self, data):
        fig_width = 2
        fig_height = fig_width
        self.fig = plt.figure(
            figsize=(fig_width, fig_height),
            num=self.name,
            facecolor=dark,
            edgecolor=dark,
            dpi=300,
        )
        border = .1
        self.ax = self.fig.add_axes((
            border, border, 1 - 2 * border, 1 - 2 * border),
            frameon=False,
        )
        self.ax.tick_params(
            bottom=False,
            top=False,
            left=False,
            right=False)
        self.ax.tick_params(
            labelbottom=False,
            labeltop=False,
            labelleft=False,
            labelright=False)

        n_rows, n_cols, n_chans = data.shape
        self.space_size = np.max([n_rows, n_cols, n_chans]) * 3 ** .5
        # The largest allowed marker diameter, in inches
        self.max_marker_size = (
            np.minimum(fig_width, fig_height) *
            (1 - 2 * border) /
            self.space_size *
            .44  # Max fraction of space a marker can occupy
            # .22  # Max fraction of space a marker can occupy
        )

        self.ax.set_xlim(0, self.space_size + 1)
        self.ax.set_ylim(0, self.space_size + 1)

        x_center = (n_cols - 1) / 2
        x_centered = np.arange(n_cols) - x_center
        self.xs = np.tile(
            x_centered[np.newaxis, :, np.newaxis], (n_rows, 1, n_chans))
        y_center = (n_rows - 1) / 2
        y_centered = np.arange(n_rows)[::-1] - y_center
        self.ys = np.tile(
            y_centered[:, np.newaxis, np.newaxis], (1, n_cols, n_chans))
        z_center = (n_chans - 1) / 2
        z_centered = np.arange(n_chans)[::-1] - z_center
        self.zs = np.tile(
            z_centered[np.newaxis, np.newaxis, :], (n_rows, n_cols, 1))

        self.azimuth = np.pi / 3
        self.elevation = np.pi / 6
        self.viewing_distance = 1.5 * self.space_size
        xp, yp, zp = rotate(
            self.xs, self.ys, self.zs,
            self.azimuth, self.elevation, self.viewing_distance)

        x_offset = xp + self.space_size / 2
        y_offset = yp + self.space_size / 2
        sizes = scatter_size(np.abs(data) * self.max_marker_size)
        # Closer elements should look larger.
        sizes_p = sizes * self.viewing_distance / (self.viewing_distance - zp)
        self.positive_elements = self.ax.scatter(
            x_offset.ravel(),
            y_offset.ravel(),
            c=gold,
            edgecolors="none",
            s=sizes_p.ravel(),
            # alpha=.8,
        )
        self.negative_elements = self.ax.scatter(
            x_offset.ravel(),
            y_offset.ravel(),
            c=light_blue,
            edgecolors="none",
            s=sizes_p.ravel(),
            # alpha=.8,
        )

        self.scale_text = self.ax.text(
            0,
            0,
            "scale: 0.0",
            horizontalalignment="left",
            verticalalignment="top",
            color=orange,
            fontsize=3,
            fontfamily="sans-serif",
        )

        plt.ion()
        plt.show()

    def update(self, new_data):
        if self.fig is None:
            self.initialize(new_data)
        self.i_step += 1

        scale = np.max(np.abs(new_data))
        data = new_data.ravel() / (scale + 1e-20)

        # self.azimuth = np.pi / 3  + np.pi / 13 * np.sin(.01 * self.i_step)
        self.azimuth = self.i_step * self.rotation_speed
        self.elevation = np.pi / 12 + np.pi / 24 * np.sin(.0022 * self.i_step)
        xp, yp, zp = rotate(
            self.xs, self.ys, self.zs,
            self.azimuth, self.elevation, self.viewing_distance)
        x_offset = xp.ravel() + self.space_size / 2
        y_offset = yp.ravel() + self.space_size / 2
        locations = np.concatenate(
            (x_offset[:, np.newaxis], y_offset[:, np.newaxis]), axis=1)

        positive_data = np.maximum(0, data)
        negative_data = np.maximum(0, -data)
        positive_sizes = scatter_size(positive_data * self.max_marker_size)
        negative_sizes = scatter_size(negative_data * self.max_marker_size)

        # Closer elements should look larger.
        positive_sizes_p = (
            positive_sizes *
            self.viewing_distance /
            (self.viewing_distance - 3 * zp.ravel()))
        negative_sizes_p = (
            negative_sizes *
            self.viewing_distance /
            (self.viewing_distance - 3 * zp.ravel()))
        self.positive_elements.set_sizes(positive_sizes_p)
        self.negative_elements.set_sizes(negative_sizes_p)
        self.positive_elements.set_offsets(locations)
        self.negative_elements.set_offsets(locations)

        self.scale_text.set_text(f"scale: {scale:.03}")
        self.fig.canvas.flush_events()


def scatter_size(diameters):
    """
    Convert a set of diameters in inches to Matplotlib scatterplot sizes,
    diameter in points, squared.
    """
    # scale = (np.max(diameters))
    # diameters = np.sqrt(diameters / (scale + 1e-20)) * scale
    diameters_in_points = diameters * 72
    sizes = diameters_in_points ** 2
    return sizes


def rotate(xs, ys, zs, azimuth, elevation, viewing_distance):

    # Rotate for azimuth
    xq = xs * np.cos(azimuth) + zs * np.sin(azimuth)
    zq = -xs * np.sin(azimuth) + zs * np.cos(azimuth)

    # Rotate for elevation
    yq = ys * np.cos(elevation) - zq * np.sin(elevation)

    # Parallax
    # https://en.wikipedia.org/wiki/Parallax
    parallax =  1 + zq / (viewing_distance - zq)
    xp = xq * parallax
    yp = yq * parallax
    return xp, yp, zq
